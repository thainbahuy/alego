<?php

namespace App\Model\Admin;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubMenu extends Model
{
    protected $table = 'sub_menu';

    protected $fillable = ['name', 'menu_id', 'slug'];

    protected $primaryKey = 'sub_menu_id';

    use Sluggable;

    public function getAllSubMenu()
    {
        return DB::table('sub_menu')
            ->select('sub_menu_id', 'sub_menu.name', 'menu.name as menu')
            ->join('menu', 'menu.menu_id', '=', 'sub_menu.menu_id')
            ->get();
    }

    public function deleteSubMenu($id)
    {
        return DB::table($this->table)->where('sub_menu_id', $id)->delete();
    }

    public function addNewSubMenu($menuId, $name)
    {
        $subMenu = new SubMenu();
        $subMenu->name = $name;
        $subMenu->menu_id = $menuId;

        return $subMenu->save();
    }

    public function updateSubMenu($id, $menuId, $name)
    {
        $subMenu = SubMenu::findOrFail($id);
        $subMenu->name = $name;
        $subMenu->menu_id = $menuId;

        return $subMenu->save();
    }

    public function getSubMenuById($id)
    {
        return DB::table($this->table)->where('sub_menu_id', $id)
            ->select('*')
            ->first();
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
