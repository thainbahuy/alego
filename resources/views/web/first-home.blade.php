<!doctype html>
<html lang="en-US" class="no-js">
<head>
    @include('web.layout_common.head')
    <title>Alego Story</title>
</head>

<body
    class="page-template page-template-template-normal page-template-template-normal-php page page-id-352 wp-custom-logo light-navigation sticky-header sticky-footer wpb-js-composer js-comp-ver-4.11.1 vc_responsive">

<div class="fade">
    <div class="inner"></div>
</div>

<header id="header">
    @include('web.layout_common.header',['data'=>$menu])
</header>
<main id="main" class="bg-lines">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            @foreach($listBackground as $item)
                <div class="swiper-slide">
                    <img src="{{Helpers::convertJsonToArray($item->image_link)['link']}}" alt="">
                </div>
            @endforeach
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev swiper-button-white"></div>
        <div class="swiper-button-next swiper-button-white"></div>
    </div>
    <div class="masonry masonry-cards" data-columns="3" data-gutter="30">
        @include('data_event_type')
    </div>
</main>
@include('modal-video')
<section class="bg-lines cta pr-7 pb-13 pl-7">
    <div class="container p-0"><h5 class="mb-8"></h5>
        <div class="row no-gutters align-items-center p-5">
            <div class="col-md">
                <h2 data-animation="fade-in-bottom 800ms 200ms" class="m-0">Let&#039;s work together</h2>
            </div>
            <div class="col-auto">
                <a data-animation="fade-in-bottom 500ms 500ms" href="{{url('contact-us')}}" class="button style-7 mb-0">@lang('Message.Contact')</a>
            </div>
        </div>
    </div>
</section>
<footer id="footer">
    @include('web.layout_common.footer')
</footer>
@include('web.layout_common.footerScript')
<script src="{{asset('website/library/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('website/js/event-type.js')}}"></script>
<script>
    var mySwiper = new Swiper ('.swiper-container', {
        autoHeight : true,
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        calculateHeight:true,

        // If we need pagination
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        effect : "coverflow",
        autoplay: {
            delay: 2000,
        },
    });
</script>
</body>
</html>

<!-- Localized -->
