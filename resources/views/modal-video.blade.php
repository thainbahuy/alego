<!-- The Modal -->
<div id="previewModal" class="modal animate show" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h6 id="title" class="modal-title text-center">Modal Heading</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <iframe id="video_link" width="100%" height="100%" src="https://www.youtube.com/embed/yrPIlWfM75o" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
